from django.contrib import admin
from . import models


admin.site.register(models.Course)
admin.site.register(models.CourseMember)
admin.site.register(models.Material)
admin.site.register(models.Rating)
