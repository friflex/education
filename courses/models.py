from django.db import models
from django.contrib.auth.models import User


class Course(models.Model):
    name = models.CharField(max_length=128)
    description = models.TextField(blank=True, default='')
    users = models.ManyToManyField(User, related_name='courses', through='CourseMember')

    def __str__(self):
        return self.name


class CourseMember(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='course')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='course_member')

    def __str__(self):
        return self.course.name

    class Meta:
        unique_together = ('course', 'user')


class Material(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='materials')
    name_material = models.CharField(max_length=128)
    file = models.FileField(upload_to='documents')

    def __str__(self):
        return self.course.name


class Rating(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE,  related_name='rating')
    user = models.ForeignKey(User, on_delete=models.SET_DEFAULT, default='')
    mark = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.course.name

    class Meta:
        unique_together = ('course', 'user')
