# Generated by Django 4.0.1 on 2022-02-01 07:48

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('courses', '0002_remove_coursemembers_review_alter_course_description_and_more'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='CourseMembers',
            new_name='CourseMember',
        ),
    ]
