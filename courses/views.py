from . import serializers
from django.db.models import Q
from rest_framework import generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from . import models


class CoursesListView(APIView):
    """Получение списка курсов"""

    def get(self, request):
        courses = models.Course.objects.all()
        serializer = serializers.CourseListSerializer(courses, many=True)
        return Response(serializer.data)


class CourseDetailView(APIView):
    """Просмотр курса"""

    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk):
        course_member = models.CourseMember.objects.filter(Q(course=pk), Q(user=request.user))
        if course_member:
            course = models.Course.objects.get(id=pk)
            serializer = serializers.CourseDetailSerializer(course)
            return Response(serializer.data)
        else:
            return Response(status=403, data='Вы не записаны на курс')


class AddCourseRatingView(APIView):
    """Выставление отметки курсу"""

    permission_classes = [permissions.IsAuthenticated]

    def post(self, request):
        serializer = serializers.RatingSerializer(data=request.data)
        if serializer.is_valid():
            try:
                serializer.save(user=request.user)
            except Exception as e:
                print(e)
                return Response(status=400, data='Вы уже выставили оценку данному курсу')
            return Response(status=201)
        else:
            return Response(status=400)


class SubscribeCourseView(APIView):
    """Записаться на курс"""

    permission_classes = [permissions.IsAuthenticated]

    def post(self, request):
        serializer = serializers.CourseMemberDetailSerializer(data=request.data)
        if serializer.is_valid():
            try:
                serializer.save(user=request.user)
            except Exception as e:
                print(e)
                return Response(status=400, data='Вы уже подписаны на данный курс')
            return Response(status=201)
        else:
            return Response(status=400)
