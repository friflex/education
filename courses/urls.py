from . import views
from django.urls import path
from django.views.generic.base import RedirectView


urlpatterns = [
    path('', RedirectView.as_view(url='/courses/')),
    path('courses/', views.CoursesListView.as_view()),
    path('courses/<int:pk>', views.CourseDetailView.as_view()),
    path('rating/', views.AddCourseRatingView.as_view()),
    path('subscribe/',  views.SubscribeCourseView.as_view()),
]
