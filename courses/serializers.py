from rest_framework import serializers
from . import models


class CourseListSerializer(serializers.ModelSerializer):
    """Список курсов"""

    class Meta:
        model = models.Course
        fields = ('name', 'description')


class CourseMemberDetailSerializer(serializers.ModelSerializer):
    """Запись на курс"""
    class Meta:
        model = models.CourseMember
        fields = ('course',)

    def create(self, validated_data):
        course_member = models.CourseMember.objects.create(
            user=validated_data.get('user', None),
            course=validated_data.get('course', None)
        )
        return course_member


class MaterialDetailSerializer(serializers.ModelSerializer):
    """Просмотр материалов курса"""

    class Meta:
        model = models.Material
        fields = ('name_material', 'file')


class RatingSerializer(serializers.ModelSerializer):
    """Выставление оценки"""

    class Meta:
        model = models.Rating
        fields = ('course', 'mark')

    def create(self, validated_data):
        rating = models.Rating.objects.create(
            user=validated_data.get('user', None),
            course=validated_data.get('course', None),
            mark=validated_data.get('mark', None)
        )
        return rating


class CourseDetailSerializer(serializers.ModelSerializer):
    """Просмотр курса"""
    materials = MaterialDetailSerializer(read_only=True,  many=True)

    class Meta:
        model = models.Course
        fields = ('name', 'description', 'materials')
